package io.gitlab.oybek.hangman

import cats.effect.{ExitCode, IO, IOApp}
import io.gitlab.oybek.hangman.service.impl.{ConsoleServiceImpl, DictServiceImpl, HangmanServiceImpl}
import io.gitlab.oybek.hangman.service.{ConsoleService, DictService, HangmanService}

// $COVERAGE-OFF$
object Hangman extends IOApp {
  val dictService: DictService[IO] = new DictServiceImpl[IO]
  val consoleService: ConsoleService[IO] = new ConsoleServiceImpl[IO]
  val hangmanService: HangmanService[IO] = new HangmanServiceImpl[IO](dictService, consoleService)

  override def run(args: List[String]): IO[ExitCode] =
    for {
      _ <- hangmanService.play
    } yield ExitCode.Success
}
// $COVERAGE-ON$
