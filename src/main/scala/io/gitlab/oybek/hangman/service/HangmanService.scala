package io.gitlab.oybek.hangman.service

trait HangmanService[F[_]] {
  def play: F[Unit]
}
