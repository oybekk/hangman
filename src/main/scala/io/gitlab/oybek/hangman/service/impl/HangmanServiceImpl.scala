package io.gitlab.oybek.hangman.service.impl

import cats.Monad
import cats.implicits.{toFlatMapOps, toFunctorOps}
import io.gitlab.oybek.hangman.model.GameState
import io.gitlab.oybek.hangman.service.{ConsoleService, DictService, HangmanService}

class HangmanServiceImpl[F[_]: Monad](dictService: DictService[F],
                                      consoleService: ConsoleService[F]) extends HangmanService[F] {

  override def play: F[Unit] =
    for {
      word <- dictService.randomWord
      _ <- turn(GameState(word, List.empty[Char]))
    } yield ()

  def turn(game: GameState): F[Unit] =
    consoleService
      .writeLines(s"The word: ${game.maskedWord}")
      .flatMap(_ =>
        game match {
          case _ if game.mistakeNum >= maxMistakeNumber =>
            consoleService.writeLines("You lost!")

          case _ if game.wordFound =>
            consoleService.writeLines("You won!")

          case GameState(word, letters) =>
            for {
              _ <- consoleService.writeString("Guess a letter:\n< ")
              letter <- consoleService.readChar
              message = if (word.contains(letter)) {
                "Hit!"
              } else {
                s"Missed, mistake ${game.mistakeNum + 1} out of $maxMistakeNumber"
              }
              _ <- consoleService.writeLines(message)
              _ <- turn(GameState(word, letter :: letters))
            } yield ()
        }
      )

  private val maxMistakeNumber = 5
}
