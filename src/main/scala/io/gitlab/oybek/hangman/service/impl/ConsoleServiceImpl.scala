package io.gitlab.oybek.hangman.service.impl

import cats.effect.Sync
import cats.implicits.{toFunctorOps, toTraverseOps}
import cats.instances.list._
import io.gitlab.oybek.hangman.service.ConsoleService

import scala.io.StdIn

// $COVERAGE-OFF$
class ConsoleServiceImpl[F[_]: Sync] extends ConsoleService[F] {

  override def writeLines(lines: String*): F[Unit] =
    lines.toList.traverse(line => Sync[F].delay(println(line))).void

  override def writeString(line: String): F[Unit] =
    Sync[F].delay(print(line))

  override def readChar: F[Char] =
    Sync[F].delay(StdIn.readChar())
}
// $COVERAGE-ON$
