package io.gitlab.oybek.hangman.service

trait ConsoleService[F[_]] {
  def readChar: F[Char]
  def writeLines(lines: String*): F[Unit]
  def writeString(string: String): F[Unit]
}
