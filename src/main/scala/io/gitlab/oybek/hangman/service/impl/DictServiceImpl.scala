package io.gitlab.oybek.hangman.service.impl

import cats.Applicative
import cats.implicits.catsSyntaxApplicativeId
import io.gitlab.oybek.hangman.service.DictService

import scala.util.Random

// $COVERAGE-OFF$
class DictServiceImpl[F[_]: Applicative] extends DictService[F] {
  override def randomWord: F[String] =
    words(Random.nextInt(words.length)).pure[F]

  private val words = Vector("forest", "hello", "banana", "kingkong")
}
// $COVERAGE-ON$
