package io.gitlab.oybek.hangman.service

trait DictService[F[_]] {
  def randomWord: F[String]
}
