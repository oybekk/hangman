package io.gitlab.oybek.hangman.model

case class GameState(word: String, letters: List[Char]) {
  def maskedWord: String =
    word.foldLeft("") {
      (word, c) => word + letters.find(_ == c).getOrElse('*')
    }

  def mistakeNum: Int =
    letters.count(!word.contains(_))

  def wordFound: Boolean =
    word.forall(letters.contains)
}
