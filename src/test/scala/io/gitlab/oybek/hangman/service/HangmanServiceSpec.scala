package io.gitlab.oybek.hangman.service

import cats.Id
import io.gitlab.oybek.hangman.service.impl.HangmanServiceImpl
import org.scalamock.scalatest.MockFactory
import org.scalatest.funsuite.AnyFunSuite

class HangmanServiceSpec extends AnyFunSuite with MockFactory {
  test("Hangman won scenario") {
    inSequence {
      (() => mockDictService.randomWord).expects().returns("hello").once()

      (mockConsoleService.writeLines _).expects(Seq(s"The word: *****")).returns(()).once()
      (mockConsoleService.writeString _).expects(s"Guess a letter:\n< ").returns(()).once()
      (() => mockConsoleService.readChar).expects().returns('a').once()
      (mockConsoleService.writeLines _).expects(Seq(s"Missed, mistake 1 out of 5")).returns(()).once()

      (mockConsoleService.writeLines _).expects(Seq(s"The word: *****")).returns(()).once()
      (mockConsoleService.writeString _).expects(s"Guess a letter:\n< ").returns(()).once()
      (() => mockConsoleService.readChar).expects().returns('b').once()
      (mockConsoleService.writeLines _).expects(Seq(s"Missed, mistake 2 out of 5")).returns(()).once()

      (mockConsoleService.writeLines _).expects(Seq(s"The word: *****")).returns(()).once()
      (mockConsoleService.writeString _).expects(s"Guess a letter:\n< ").returns(()).once()
      (() => mockConsoleService.readChar).expects().returns('e').once()
      (mockConsoleService.writeLines _).expects(Seq(s"Hit!")).returns(()).once()

      (mockConsoleService.writeLines _).expects(Seq(s"The word: *e***")).returns(()).once()
      (mockConsoleService.writeString _).expects(s"Guess a letter:\n< ").returns(()).once()
      (() => mockConsoleService.readChar).expects().returns('o').once()
      (mockConsoleService.writeLines _).expects(Seq(s"Hit!")).returns(()).once()

      (mockConsoleService.writeLines _).expects(Seq(s"The word: *e**o")).returns(()).once()
      (mockConsoleService.writeString _).expects(s"Guess a letter:\n< ").returns(()).once()
      (() => mockConsoleService.readChar).expects().returns('l').once()
      (mockConsoleService.writeLines _).expects(Seq(s"Hit!")).returns(()).once()

      (mockConsoleService.writeLines _).expects(Seq(s"The word: *ello")).returns(()).once()
      (mockConsoleService.writeString _).expects(s"Guess a letter:\n< ").returns(()).once()
      (() => mockConsoleService.readChar).expects().returns('h').once()
      (mockConsoleService.writeLines _).expects(Seq(s"Hit!")).returns(()).once()

      (mockConsoleService.writeLines _).expects(Seq(s"The word: hello")).returns(()).once()
      (mockConsoleService.writeLines _).expects(Seq(s"You won!")).returns(()).once()
    }

    hangmanServiceImpl.play
  }

  test("Hangman lost scenario") {
    inSequence {
      (() => mockDictService.randomWord).expects().returns("banana").once()

      (mockConsoleService.writeLines _).expects(Seq(s"The word: ******")).returns(()).once()
      (mockConsoleService.writeString _).expects(s"Guess a letter:\n< ").returns(()).once()
      (() => mockConsoleService.readChar).expects().returns('x').once()
      (mockConsoleService.writeLines _).expects(Seq(s"Missed, mistake 1 out of 5")).returns(()).once()

      (mockConsoleService.writeLines _).expects(Seq(s"The word: ******")).returns(()).once()
      (mockConsoleService.writeString _).expects(s"Guess a letter:\n< ").returns(()).once()
      (() => mockConsoleService.readChar).expects().returns('y').once()
      (mockConsoleService.writeLines _).expects(Seq(s"Missed, mistake 2 out of 5")).returns(()).once()

      (mockConsoleService.writeLines _).expects(Seq(s"The word: ******")).returns(()).once()
      (mockConsoleService.writeString _).expects(s"Guess a letter:\n< ").returns(()).once()
      (() => mockConsoleService.readChar).expects().returns('z').once()
      (mockConsoleService.writeLines _).expects(Seq(s"Missed, mistake 3 out of 5")).returns(()).once()

      (mockConsoleService.writeLines _).expects(Seq(s"The word: ******")).returns(()).once()
      (mockConsoleService.writeString _).expects(s"Guess a letter:\n< ").returns(()).once()
      (() => mockConsoleService.readChar).expects().returns('n').once()
      (mockConsoleService.writeLines _).expects(Seq(s"Hit!")).returns(()).once()

      (mockConsoleService.writeLines _).expects(Seq(s"The word: **n*n*")).returns(()).once()
      (mockConsoleService.writeString _).expects(s"Guess a letter:\n< ").returns(()).once()
      (() => mockConsoleService.readChar).expects().returns('m').once()
      (mockConsoleService.writeLines _).expects(Seq(s"Missed, mistake 4 out of 5")).returns(()).once()

      (mockConsoleService.writeLines _).expects(Seq(s"The word: **n*n*")).returns(()).once()
      (mockConsoleService.writeString _).expects(s"Guess a letter:\n< ").returns(()).once()
      (() => mockConsoleService.readChar).expects().returns('o').once()
      (mockConsoleService.writeLines _).expects(Seq(s"Missed, mistake 5 out of 5")).returns(()).once()

      (mockConsoleService.writeLines _).expects(Seq(s"The word: **n*n*")).returns(()).once()
      (mockConsoleService.writeLines _).expects(Seq(s"You lost!")).returns(()).once()
    }

    hangmanServiceImpl.play
  }
  //
  private lazy val mockDictService: DictService[Id] = mock[DictService[Id]]
  private lazy val mockConsoleService: ConsoleService[Id] = mock[ConsoleService[Id]]

  private lazy val hangmanServiceImpl: HangmanService[Id] =
    new HangmanServiceImpl[Id](mockDictService, mockConsoleService)
}
