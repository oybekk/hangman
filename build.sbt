
name := "hangman"

version := "0.1"

scalaVersion := "2.13.6"

coverageEnabled := true
coverageFailOnMinimum := true
coverageMinimumStmtTotal := 100

libraryDependencies ++= Seq(
  "org.typelevel" %% "cats-core"   % "2.5.0",
  "org.typelevel" %% "cats-effect" % "2.5.0",
  "org.scalatest" %% "scalatest"   % "3.2.9" % Test,
  "org.scalamock" %% "scalamock"   % "5.1.0" % Test
)

assemblyMergeStrategy in assembly := {
  case PathList("META-INF", _ @ _*) => MergeStrategy.discard
  case _ => MergeStrategy.first
}
